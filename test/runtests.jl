using Cent2Profile
using Test
#using DecisionTree
#using JLD2
#using FileIO
using Pkg


try
    using MS_Import
catch
    @warn("MS_Import is being installed")
    Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ms_import.jl/src/master/"))
     using MS_Import
end



@testset "Checking Depenedencies" begin
    #using Base


    path=""
    filenames=["test_file.mzXML"]
    mz_thresh=[200,300]

    chrom=import_files(path,filenames,mz_thresh)


    @test chrom["MS1"]["Rt"][1] == 0.022

end




@testset "Cent2Profile.jl" begin
    path=""
    filenames=["test_file.mzXML"]
    mz_thresh=[200,300]
    res = 20000
    min_int = 10000

    chrom=import_files(path,filenames,mz_thresh,5000)
    mz_val = chrom["MS1"]["Mz_values"]
    mz_int = chrom["MS1"]["Mz_intensity"]

    mz_val_cent,mz_int_cent,dm_c = centroid(mz_val,mz_int,min_int,res)

    mz_prof_pred,int_prof_pred=cent2profile(mz_val_cent,mz_int_cent)

    dm_pre=dmz_pred_batch(mz_val_cent,mz_int_cent)

    #dmz_pred_batch()

    #println(dm_pre)

    @test dm_c[2][1] == 0.0085
    @test mz_val_cent[5,2] == 216.99493408203125
    @test mz_prof_pred[5,3] == 212.0577736882249
    @test dm_pre[10,2] == 12.807593960928127

end
