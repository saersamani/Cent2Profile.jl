
using DataFrames
using ProgressMeter
using LsqFit
using Statistics
using BenchmarkTools
using FileIO
#using DecisionTree
using PyCall 
using ScikitLearn
using Conda

#Conda.add("joblib")
#jl = pyimport("joblib")

#@sk_import linear_model: LogisticRegression
@sk_import ensemble: RandomForestRegressor



################################################################
# This module is to convert centroided HRMS data to profile data.
#
#
################################################################

################################################################
# Function for predicting the profile

function prof_pred(features,dm_per,int_c,f)

    @.model_1(x, p) = (1/(p[2]*sqrt(pi*2))) * exp(-(x-p[1])^2 / (2*p[2]^2))
    @.model_2(x, p) = (1/pi)*((p[2])/((x - p[1])^2 + (p[2])^2))


    p=zeros(2)
    mz_pred = 0;
    int_pred = 0;

    for j=1:length(dm_per)

        #p[1] = int_c[j]
        p[1] = features[j,2]
        p[2] = dm_per[j]/2000
        #ms_vec = features[j,2]-(2*p[2]):p[2]:features[j,2]+(2*(2*p[2]))
        ms_vec = range(features[j,2]-(2*p[2]), stop=features[j,2]+(2*(2*p[2])), length=8)
        if f == "gs"
            y_r=model_1(ms_vec,p)
        elseif f == "scd"
            y_r=model_2(ms_vec,p)
        else
            println("At the moment only Gassuian (gs) and Cauchy (scd) are enabled.
            Your data will be processed with the Gaussian model")
            y_r=model_1(ms_vec,p)
        end


        y_pred = int_c[j] * (y_r/maximum(y_r))
        y_pred =vcat(0,y_pred,0)
        ms_vec = vcat(ms_vec[1]-p[2],ms_vec,ms_vec[end]+p[2])
        # oplot( ms_vec,y_pred,"g:")
        # oplot( ms_vec,y_pred_sc,"b-.")

        mz_pred = hcat(mz_pred,ms_vec')
        int_pred = hcat(int_pred,y_pred')

    end

    return(mz_pred,int_pred)


end




################################################################
# Wrapping function

function cent2profile(mz_val_cent,mz_int_cent,fun="gs")

    mm=pathof(Cent2Profile)
    path2aux=joinpath(mm[1:end-15],"QTOF_model.joblib")
    println("The model is being loaded. This may take a few minutes.")
    #println(path2aux)
    #model = jl.load("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/QTOF_model.joblib")
    model = jl.load(path2aux)
    model.verbose = 0;

    mz_prof_pred = zeros(size(mz_val_cent,1),100*size(mz_val_cent,2));
    int_prof_pred = zeros(size(mz_val_cent,1),100*size(mz_val_cent,2));

    # i=100
    @showprogress 1 "The data is being converted..." for i =1:size(mz_val_cent,1)
        sleep(0.5)
    #for i=1:size(mz_val_cent,1)
    #    println(i)
        ms = mz_val_cent[i,:];
        ms_int = mz_int_cent[i,:];
        if length(ms_int[ms_int .>0])>0
            rel_ms_int = 100 .* (ms_int ./ maximum(ms_int));
            rt_fact = 100 * i/size(mz_val_cent,1)
            features = hcat(rt_fact * ones(length(ms[ms .>0]),1),ms[ms .>0],rel_ms_int[ms .>0])
            dm_per = predict(model, features);

            # fun = "scd"
            # fun = "gs"
            mz_pred,int_pred = prof_pred(features,dm_per,ms_int[ms .>0],fun);

            # stem(ms,ms_int)
            # oplot(mz_val[i,:],mz_int[i,:],"r")
            # oplot(mz_pred,int_pred,"b-.")
            # oplot(mz_pred,int_pred,"g--")
            # xlim(343.18,343.4)
            # ylim(500,2500)
            # legend([" ","Centrodied data","Profile data", "Predicted profile Cauchy dist", "Predicted profile Gaussian dist"])
            # xlabel("m/z (Da)")
            # ylabel("Intensity")
            # savefig("predicted_prof3.png")

            mz_prof_pred[i,1:length(mz_pred)] =  mz_pred[:];
            int_prof_pred[i,1:length(mz_pred)] = int_pred[:];
        end

    end
    model = []
    GC.gc()
    return(mz_prof_pred,int_prof_pred)


end



################################################################
# Function to predict the Delta m/z


function dmz_pred(mass,rel_int,ret_fact)

    mm=pathof(Cent2Profile)
    path2aux=joinpath(mm[1:end-15],"QTOF_model.joblib")
    println("The model is being loaded. This may take a few minutes.")
    # model = jl.load("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/reg.joblib")
    model = jl.load(path2aux)
    model.verbose = 0;


    features = hcat(ret_fact,mass,rel_int)
    dm_pre = predict(model, features);

    return dm_pre

end



################################################################
# Function to predict the Delta m/z


function dmz_pred_batch(mz_val_cent,mz_int_cent)

    mm=pathof(Cent2Profile)
    path2aux=joinpath(mm[1:end-15],"QTOF_model.joblib")
    println("The model is being loaded. This may take a few minutes.")
    # model = jl.load("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/QTOF_model.joblib")
    model = jl.load(path2aux)
    model.verbose = 0;


    dm_pre = zeros(size(mz_val_cent));
    

    #println(dm_pre)

    @showprogress 1 "The dm/z are being estimated ..." for i =1:size(mz_val_cent,1)
        sleep(0.5)
    #for i=1:size(mz_val_cent,1)
    #   println(i)
        ms = mz_val_cent[i,:];
        ms_int = mz_int_cent[i,:];
        if length(ms_int[ms_int .>0])>0
            rel_ms_int = 100 .* (ms_int ./ maximum(ms_int));
            rt_fact = 100 * i/size(mz_val_cent,1)
            features = hcat(rt_fact * ones(length(ms[ms .>0]),1),ms[ms .>0],rel_ms_int[ms .>0])
            dm_pre[i,ms .>0] = predict(model, features);


        end

    end
    model = []
    GC.gc()



    return dm_pre

end


 ################################################################
# testing area
#
"""

using MS_Import
using Plots
#using NaNStatistics 
include("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/Centroid.jl")


# import parameters
pathin="/Volumes/SAER HD/Data/Temp_files/Phil/Cal"
filenames=["100PPB DRUGS.mzXML"]
mz_thresh=[0,550]
int_thresh=250

ch = import_files(pathin,filenames,mz_thresh,int_thresh)


chrom = deepcopy(ch)
mz_val = chrom["MS1"]["Mz_values"]
mz_int = chrom["MS1"]["Mz_intensity"];

mz_val1 = chrom["MS1"]["Mz_values"];
mz_int1 = chrom["MS1"]["Mz_intensity"];

mz_val1[mz_int1 .<= 1000] .= 0
mz_int1[mz_int1 .<= 1000] .= 0





# mz_val_cent = chrom["MS1"]["Mz_values"];
# mz_int_cent = chrom["MS1"]["Mz_intensity"];

#plot(sum(mz_int,dims=2))

# cent parameters
r_thresh = 0.8
res = 20000
min_int = 1000
s2n = 1.5
min_p_w = 0.02
filename = split(filenames[1],".")[1] * "_cent.csv"

#fun = "gs"
GC.gc()


mz_val_cent,mz_int_cent,dm_c = centroid(mz_val,mz_int,min_int,res,r_thresh,min_p_w,s2n);




#table = chrom2csv(mz_val_cent,mz_int_cent,dm_c,filename);

# mz_int_f = mz_int .- min_int .* ones(size(mz_int))
# mz_int[findall(x-> x<min_int,mz_int)] .= 0

mz_prof_pred,int_prof_pred = cent2profile(mz_val_cent,mz_int_cent);












plot(sum(mz_int1,dims=2),label ="Measured TIC")
plot!(sum(int_prof_pred,dims=2),label ="Predicted TIC")
xlabel!("Scan Number")
ylabel!("Intensity")




p111 = plot(mz_val[1401,:],mz_int[1401,:],label ="Scan 1400")
xlims!(80,510)
xlabel!("m/z values")
ylabel!("Intensity")
annotate!((100,2.95*100000,"(a)"))

p112 = plot(mz_val[1401,:],mz_int[1401,:],label ="Scan 1400")
plot!([80,510],[1000,1000],color=:red,label ="Intensity Threshold")
xlims!(80,510)
ylims!(0,2000)

xlabel!("m/z values")
ylabel!("Intensity")

annotate!((100,1900,"(b)"))

plot(p111,p112, layout =(2,1))
plot!(size=(550,650))



Plots.savefig("Scan1400.pdf")





p1111 = plot(mz_val1[1401,:],mz_int1[1401,:],label ="Raw data")
xlims!(343.32,343.38)
Plots.scatter!(mz_val1[1401,:],mz_int1[1401,:],label ="Raw data")
plot!([343.337,343.337],[0,295063],label ="Centroided data")
plot!(mz_prof_pred[1401,:],int_prof_pred[1401,:],label ="Predicted profile",line = (:dot,2))
Plots.scatter!(mz_prof_pred[1401,:],int_prof_pred[1401,:],label ="Predicted profile",color = :green)
ylims!(2000,306063)
ylabel!("Intensity")
xlabel!("m/z values")
annotate!((343.325,295000,"(a)"))


p1112 = plot(sum(mz_int1,dims=2),label ="Raw data")
plot!(sum(int_prof_pred,dims=2),label ="Predicted profiles")
xlabel!("Scan Number")
annotate!((120,2.95*10000000,"(b)"))

plot(p1111,p1112, layout =2)
plot!(size=(780,450))

Plots.savefig("TIC.pdf")


#

intt = int_prof_pred[1401,findall(x -> 343.37 >= x >=343.3,mz_prof_pred[1401,:] )]
mtt = mz_prof_pred[1401,findall(x -> 343.37 >= x >=343.3,mz_prof_pred[1401,:] )]


intt0 = mz_int1[1401,findall(x -> 343.37 >= x >=343.3,mz_val1[1401,:] )]
mtt0 = mz_val1[1401,findall(x -> 343.37 >= x >=343.3,mz_val1[1401,:] )]


Plots.scatter(mtt,intt)
Plots.scatter!(mtt0,intt0)




#
# using Winston



# Winston.plot(sum(int_prof_pred,dims=2))
# oplot(sum(mz_int,dims=2),"r")
# legend(["Predicted TIC","Experimental TIC"])
# xlabel("Scan number")
# ylabel("Intensity")
# savefig("TIC.png")
"""
